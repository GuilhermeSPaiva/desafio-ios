//
//  PullViewController.swift
//  desafio-ios
//
//  Created by Guilherme Paiva on 23/01/2018.
//  Copyright © 2018 Guilherme Paiva. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class PullViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var repoName: UILabel!
    @IBOutlet weak var pullState: UILabel!
    
    var repoURL = ""
    var repo = ""
    var opened = 0
    var closed = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        repoName.text = repo
        downloadPulls()
    }

    override func viewWillDisappear(_ animated: Bool) {
        PullStaticData.pullArray.removeAll()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return PullStaticData.pullArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell = tableView.dequeueReusableCell(withIdentifier: "pullCell", for: indexPath) as! PullTableViewCell
        
        if let pullTitle = PullStaticData.pullArray[indexPath.item].pullTitle { cell.pullTitle.text = pullTitle }
        if let pullBody = PullStaticData.pullArray[indexPath.item].pullBody { cell.pullBody.text = pullBody }
        if let userImg = PullStaticData.pullArray[indexPath.item].userImg { cell.userImg.image = userImg }
        if let login = PullStaticData.pullArray[indexPath.item].login { cell.login.text = login }
        if let userName = PullStaticData.pullArray[indexPath.item].userName { cell.userName.text = userName }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let str = PullStaticData.pullArray[indexPath.item].pullURL {
            if let url = URL(string: str) { UIApplication.shared.openURL(url) }
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "pullCell", for: indexPath) as! PullTableViewCell
            
            cell.setSelected(false, animated: true)
        }
    }
    
    func configLabel() {
        let str = "\(opened) opened / \(closed) closed"
        if let newStr = str.components(separatedBy: "/").first {
            var range = NSRange()
            range.location = 0
            range.length = newStr.count
            
            let mutableStr = NSMutableAttributedString(string: str, attributes: nil)
            mutableStr.addAttribute(.foregroundColor, value: #colorLiteral(red: 0.8588235294, green: 0.5647058824, blue: 0.05098039216, alpha: 1), range: range)
            self.pullState.attributedText = mutableStr
        }
    }
}

extension PullViewController {
    
    func downloadPulls() {
        Alamofire.request(repoURL).responseArray { (response: DataResponse<[PullResponse]> ) in
            
            let gitResponseArray = response.result.value
            
            if let gitResponseArray = gitResponseArray {
                for atrib in gitResponseArray {
                    
                    let pullData = PullData()
                    
                    if let pullTitle = atrib.pullTitle { pullData.pullTitle = pullTitle }
                    if let pullDate = atrib.pullDate { pullData.pullDate = pullDate }
                    if let pullBody = atrib.pullBody { pullData.pullBody = pullBody }
                    if let pullURL = atrib.pullURL { pullData.pullURL = pullURL }
                    
                    if let pullState = atrib.pullState {
                        if pullState == "open" { self.opened += 1 }
                        else { self.closed += 1 }
                    }
                    
                    if let user = atrib.user {
                        if let login = user.login { pullData.login = login }
                        if let userID = user.userId {
                            pullData.userID = userID
                            
                            if let photoURL = user.photoURL {
                                pullData.photoURL = photoURL
                                self.downloadUserImage(url: photoURL, id: userID)
                            }
                        }
                        
                        if let userURL = user.userURL {
                            pullData.userURL = userURL
                            self.downloadUserName(userURL)
                        }
                    }
                    
                    PullStaticData.add(pullData)
                }
                
                self.tableView.reloadData()
            }
            
            self.configLabel()
        }
    }
    
    func downloadUserName(_ url: String) {
        
        Alamofire.request(url).responseObject { (response: DataResponse<UserData>) in
            
            let userData = response.result.value
            
            if let userData = userData {
                if let userID = userData.id {
                    if let userName = userData.name {
                        for pull in PullStaticData.pullArray {
                            if pull.userID == userID {
                                pull.userName = userName
                                self.tableView.reloadData()
                            }
                        }
                    }
                }
            }
        }
    }
    
    func downloadUserImage(url: String, id: Int) {
        
        Alamofire.request(url).responseData { response in
            
            if response.error == nil, let data = response.result.value {
                let image = UIImage(data: data)
                
                for pull in PullStaticData.pullArray {
                    if pull.userID == id {
                        pull.userImg = image
                        self.tableView.reloadData()
                    }
                }
            }
        }
    }
}
