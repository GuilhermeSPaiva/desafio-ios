//
//  RepoTableViewController.swift
//  desafio-ios
//
//  Created by Guilherme Paiva on 23/01/2018.
//  Copyright © 2018 Guilherme Paiva. All rights reserved.
//

// Ícones por:  https://www.flaticon.com/authors/smashicons
//              https://www.flaticon.com/authors/dave-gandy
//              http://www.freepik.com

import UIKit
import Alamofire
import AlamofireObjectMapper

class RepoTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    var down = false
    var page = 1

    override func viewDidLoad() {
        super.viewDidLoad()
        
        downloadRepos()
    }

    override func viewWillDisappear(_ animated: Bool) {
        RepoStaticData.repoArray.removeAll()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return RepoStaticData.repoArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "repoCell", for: indexPath) as! RepoTableViewCell
        
        if let repoName = RepoStaticData.repoArray[indexPath.item].repoName { cell.repoName.text = repoName }
        if let repoDescription = RepoStaticData.repoArray[indexPath.item].description { cell.repoDescription.text = repoDescription }
        if let forks = RepoStaticData.repoArray[indexPath.item].forkCount { cell.forks.text = String(forks) }
        if let stars = RepoStaticData.repoArray[indexPath.item].starCount { cell.stars.text = String(stars) }
        if let login = RepoStaticData.repoArray[indexPath.item].login { cell.login.text = login }
        if let userName = RepoStaticData.repoArray[indexPath.item].userName { cell.userName.text = userName }
        if let userImg = RepoStaticData.repoArray[indexPath.item].userImg { cell.userImage.image = userImg }

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let index = indexPath
        self.performSegue(withIdentifier: "segueToPull", sender: index)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.item >= tableView.numberOfRows(inSection: 0) - 6 {
            if down == false {
                down = true
                page += 1
                downloadRepos()
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let indexPath = sender as? IndexPath {
            if let pullURL = RepoStaticData.repoArray[indexPath.item].pullsURL {
                if let repoName = RepoStaticData.repoArray[indexPath.item].repoName {
                    if segue.identifier == "segueToPull" {
                        if let pvc = segue.destination as? PullViewController {
                            pvc.repo = repoName
                            pvc.repoURL = pullURL
                        }
                    }
                }
            }
        }
    }
}

extension RepoTableViewController {
    
    func downloadRepos() {
        
        let URL = "https://api.github.com/search/repositories?q=language:Java&sort=stars&page=\(page)"
        
        Alamofire.request(URL).responseArray(keyPath: "items") { (response: DataResponse<[GitResponse]> ) in
            
            let gitResponseArray = response.result.value
            
            if let gitResponseArray = gitResponseArray {
                for atrib in gitResponseArray {
                    
                    let repoData = RepoData()
                    
                    if let repoName = atrib.repoName { repoData.repoName = repoName }
                    if let description = atrib.description { repoData.description = description }
                    if let pullsURL = atrib.pullsURL { if let url = pullsURL.components(separatedBy: "{").first { repoData.pullsURL = url } }
                    if let starCount = atrib.starCount { repoData.starCount = starCount }
                    if let forkCount = atrib.forkCount { repoData.forkCount = forkCount }
                    
                    if let owner = atrib.owner {
                        if let login = owner.login { repoData.login = login }
                        if let authorID = owner.authorID {
                            repoData.authorID = authorID
                            
                            if let photoURL = owner.photoURL {
                                repoData.photoURL = photoURL
                                self.downloadUserImage(url: photoURL, id: authorID)
                            }
                        }
                        
                        if let userURL = owner.userURL {
                            repoData.userURL = userURL
                            self.downloadUserName(userURL)
                        }
                    }
                    
                    RepoStaticData.add(repoData)
                }
                
                self.tableView.reloadData()
                self.down = false
            }
        }
    }
    
    func downloadUserName(_ url: String) {
                
        Alamofire.request(url).responseObject { (response: DataResponse<UserData>) in
            
            let userData = response.result.value
            
            if let userData = userData {
                if let userID = userData.id {
                    if let userName = userData.name {
                        for user in RepoStaticData.repoArray {
                            if user.authorID == userID {
                                user.userName = userName
                                self.tableView.reloadData()
                            }
                        }
                    }
                }
            }
        }
    }
    
    func downloadUserImage(url: String, id: Int) {
        
        Alamofire.request(url).responseData { response in
            
            if response.error == nil, let data = response.result.value {
                let image = UIImage(data: data)
                
                for user in RepoStaticData.repoArray {
                    if user.authorID == id {
                        user.userImg = image
                        self.tableView.reloadData()
                    }
                }
            }
        }
    }
}
