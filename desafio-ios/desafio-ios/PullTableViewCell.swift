//
//  PullTableViewCell.swift
//  desafio-ios
//
//  Created by Guilherme Paiva on 24/01/2018.
//  Copyright © 2018 Guilherme Paiva. All rights reserved.
//

import UIKit

class PullTableViewCell: UITableViewCell {
    
    @IBOutlet weak var pullTitle: UILabel!
    @IBOutlet weak var pullBody: UILabel!
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var login: UILabel!
    @IBOutlet weak var userName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        userImg.layer.cornerRadius = userImg.frame.size.width / 2
        userImg.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
