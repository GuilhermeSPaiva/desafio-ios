//
//  PullStaticData.swift
//  desafio-ios
//
//  Created by Guilherme Paiva on 23/01/2018.
//  Copyright © 2018 Guilherme Paiva. All rights reserved.
//

import UIKit

class PullStaticData {
    static var pullArray = [PullData]()
    
    static func add(_ content: PullData) {
        pullArray.append(content)
    }
}
