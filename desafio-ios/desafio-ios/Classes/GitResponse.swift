//
//  GitResponse.swift
//  desafio-ios
//
//  Created by Guilherme Paiva on 22/01/2018.
//  Copyright © 2018 Guilherme Paiva. All rights reserved.
//

import Foundation
import ObjectMapper

class GitResponse: Mappable {
    var repoName: String?       // items/name
    var description: String?    // items/description
    var pullsURL: String?       // items/pulls_url
    var starCount: Int?         // items/stargazers_count
    var forkCount: Int?         // items/forks_count
    var owner: AuthorResponse?  // items/owner
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        repoName <- map["name"]
        description <- map["description"]
        pullsURL <- map["pulls_url"]
        starCount <- map["stargazers_count"]
        forkCount <- map["forks_count"]
        owner <- map["owner"]
    }
}

class AuthorResponse: Mappable {
    var login: String?          // items/owner/login
    var photoURL: String?       // items/owner/avatar_url
    var userURL: String?        // items/owner/url
    var authorID: Int?       // items/owner/id
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        login <- map["login"]
        photoURL <- map["avatar_url"]
        userURL <- map["url"]
        authorID <- map["id"]
    }
}

class PullResponse: Mappable {
    var pullTitle: String?      // title
    var pullDate: String?       // created_at
    var pullBody: String?       // body
    var pullURL: String?        // html_url
    var pullState: String?      // state
    var user: UserResponse?     // user
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        pullTitle <- map["title"]
        pullDate <- map["created_at"]
        pullBody <- map["body"]
        pullURL <- map["html_url"]
        pullState <- map["state"]
        user <- map["user"]
    }
}

class UserResponse: Mappable {
    var login: String?          // user/login
    var photoURL: String?       // user/avatar_url
    var userId: Int?            // user/id
    var userURL: String?        // user/url
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        login <- map["login"]
        photoURL <- map["avatar_url"]
        userId <- map["id"]
        userURL <- map["url"]
    }
}

class UserData: Mappable {
    var name: String?           // name
    var id: Int?                // id
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        id <- map["id"]
    }
}
