//
//  PullData.swift
//  desafio-ios
//
//  Created by Guilherme Paiva on 23/01/2018.
//  Copyright © 2018 Guilherme Paiva. All rights reserved.
//

import UIKit

class PullData {
    var pullTitle: String?
    var pullDate: String?
    var pullBody: String?
    var pullURL: String?
    var userName: String?
    var login: String?
    var photoURL: String?
    var userID: Int?
    var userImg: UIImage?
    var userURL: String?
}
