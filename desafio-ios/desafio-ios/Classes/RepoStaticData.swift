//
//  RepoStaticData.swift
//  desafio-ios
//
//  Created by Guilherme Paiva on 22/01/2018.
//  Copyright © 2018 Guilherme Paiva. All rights reserved.
//

import UIKit

class RepoStaticData {
    static var repoArray = [RepoData]()
    
    static func add(_ content: RepoData) {
        repoArray.append(content)
    }
}
