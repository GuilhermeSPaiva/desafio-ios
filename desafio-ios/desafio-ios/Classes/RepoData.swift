//
//  RepoData.swift
//  desafio-ios
//
//  Created by Guilherme Paiva on 22/01/2018.
//  Copyright © 2018 Guilherme Paiva. All rights reserved.
//

import UIKit

class RepoData {
    var repoName: String?
    var description: String?
    var pullsURL: String?
    var starCount: Int?
    var forkCount: Int?
    var login: String?
    var photoURL: String?
    var userURL: String?
    var userName: String?
    var authorID: Int?
    var userImg: UIImage?
}
